<?php
 $update = false;
 $id = 0;
 $dosen = "";
 $kelas = "";
 $matkul = "";
 $tanggal = "";

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "dosen";

    $conn = mysqli_connect($servername, $username, $password, $dbname);

    if($conn){

    }else{
        die("Connection failed : ".mysqli_connect_error());
    }
    
    if(isset($_GET["delete"])){   //Untuk hapus file
        $id = $_GET["delete"];
        $sql = "DELETE FROM `jadwal_kelas` WHERE id_jadwal = $id";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Dihapus";
        } else {
            $status = "Data Gagal Dihapus";
        }
    }

    $conn = mysqli_connect($servername, $username, $password, $dbname);

    if($conn){

    }else{
        die("Connection failed : ".mysqli_connect_error());
    }

if(isset($_POST["submit"])){
    $dosen = $_POST["dosen"];
    $kelas = $_POST["kelas"];
    $namamatkul = $_POST["matkul"];
    $tanggal = $_POST["tanggal"];

    $sql = "INSERT INTO `jadwal_kelas` (`id_jadwal`, `id_dosen`, `id_kelas`, `jadwal`, `matakuliah`) VALUES (NULL, '$dosen', '$kelas', '$tanggal', '$namamatkul');";

    if(mysqli_query($conn, $sql)){
        $status = "Data Berhasil Ditambah";
    } else {
        $status = "Data Gagal Ditambah";
    }
}

if(isset($_GET["edit"])){
    $update = true;
    $id= $_GET["edit"];
    $sql= "SELECT * FROM `jadwal_kelas` WHERE id_jadwal = $id";
    $q1 = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($q1);

    $dosen = $row["id_dosen"];
    $kelas = $row["id_kelas"];
    $namamatkul = $row["matakuliah"];
    $tanggal = $row["jadwal"];

    if($dosen == " "){
        $status = "kosong";
    }

}

if(isset($_POST["edit"])){
    $id = $_POST["id"];
    $dosen = $_POST["dosen"];
    $kelas = $_POST["kelas"];
    $namamatkul = $_POST["namamatkul"];
    $tanggal = $_POST["tanggal"];

    $sql = "UPDATE `jadwal_kelas` SET `id_dosen`='$dosen',`id_kelas`='$kelas',`jadwal`='$tanggal',`matakuliah`='$namamatkul' WHERE id_jadwal = '$id'";

    if(mysqli_query($conn, $sql)){
        $status = "Data Berhasil Ditambahkan";
    } else {
        $status = "Data Gagal Ditambahkan";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jadwal Perkuliahan</title>

    <link rel="shortcut icon" href="img/UNDIKSHA.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <style>
    .nav-link{
     font-size: 20px;
     color : #f2f2f2;
    }
</style>

</head>
<body>
    <nav class="navbar navbar-expand-sm bg-primary">
        <a class="navbar-brand" href="index.php"><img src="https://kompaspedia.kompas.id/wp-content/uploads/2020/08/logo_Universitas-Pendidikan-Ganesha-thumb.png" alt="Logo Undiksha" width="100 px"></a>
        <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="dosen.php">Dosen</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="kelas.php">Kelas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="jadwal.php">Jadwal</a>
        </li>
        </ul>
    </nav>

    <div class="container">
        <div class="row justify-content-center">
        <div class=" border-primary mt-3">
            <div class="container p-3 my-3 bg-primary text-white text-center">
                <h1>Daftar Jadwal</h1>
                <p>Silahkan Masukkan Data Jadwal Terlebih Dahulu</p>
            </div> 

    <div class="container">
        <div class="row justify-content-center">
        <div class="border-primary rounded mt-3">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <?php
                    include 'conect.php';
                    $sql= "SELECT * FROM dosen";
                    $result = mysqli_query($conn,$sql);
                ?>
                <label for="Dosen">Nama Dosen</label><br>
                <select name="dosen" id="dosen" class='form-control' required>
                <option value="">Pilih Nama Dosen</option>
                    <?php while($row = $result->fetch_assoc()):?>
                    <option value="<?php echo $row["id_dosen"];?>"><?php echo $row["nama"]; ?></option>
                    <?php endwhile; ?>
                </select><br>
                
                <?php 
                    include 'conect.php';
                    $sql= "SELECT * FROM `kelas`";
                    $result = mysqli_query($conn,$sql); 
                ?>
                <label for="kelas">Kelas</label> <br>
                <select name="kelas" id="kelas" class='form-control' required>
                <option value="">Pilih Nama Kelas</option>
                    <?php while($row = $result->fetch_assoc()) : ?>
                    <option value="<?php echo $row["id_kelas"]?>"><?php echo $row["kelas"];?></option>
                    <?php endwhile; ?>
                </select><br>

                <label for="Nama Mata Kuliah">Mata Kuliah</label><br>
                <input type="text" name="matkul" class='form-control' value="<?php //echo $nama;?>" id="matkul" required><br>
                

                <label for="tanggal">Tanggal</label><br>
                <input type="date" name="tanggal" class='form-control' required>
                <br>
                <div class="button mb5">
                    <?php if($update == true):?>
                        <input type="submit" class="btn btn-success " name="edit" value="Ubah">
                        <?php else:?>
                            <input type="submit" class="btn btn-success " name="submit" value="Simpan">
                    <?php endif; ?>
                </div> 
            </form>
        </div>
    </div>

    <?php if(isset($_POST["submit"])) : ?>
        <div class="alert alert-primary">
            <?php
                echo "$status";
            ?>
        </div>
    <?php endif;?>

    <div class="container col-15">
            <?php
                include'conect.php';
                $sql = "SELECT * FROM `jadwal_kelas` INNER JOIN dosen ON dosen.id_dosen=jadwal_kelas.id_dosen INNER JOIN  kelas ON jadwal_kelas.id_kelas=kelas.id_kelas;";
                $result = mysqli_query($conn,$sql);
            ?>
        <div class="row justify-content-center">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="table-primary">
                    <th>Nama dosen</th>
                    <th>Kelas</th>
                    <th>Jadwal</th>
                    <th>Mata Kuliah</th>
                    <th>Action</th>
                </tr>
            </thead>
            <?php while($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row["nama"];?></td>
                    <td><?php echo $row["kelas"];?></td>
                    <td><?php echo $row["jadwal"];?></td>
                    <td><?php echo $row["matakuliah"];?></td>
                    <td>
                        <a href="formjadwal.php?edit=<?php echo $row["id_jadwal"];  ?>" class="btn btn-primary" >Edit</a>
                        <a href="jadwal.php?delete=<?php echo $row["id_jadwal"]; ?>" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            <?php endwhile;?>
        </table>
        </div>
    </div>

</body>
</html>