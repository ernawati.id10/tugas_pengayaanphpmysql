<?php
$update = false;
$id = 0;
$nama = "";
$prodi="";
$fakultas="";

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "dosen";
    $conn = mysqli_connect($servername, $username, $password, $dbname);

    if(isset($_POST["submit"])){
        $kelas = $_POST["kelas"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        $sql = "INSERT INTO `kelas`(`kelas`, `prodi`, `fakultas`) VALUES ('$kelas','$prodi','$fakultas')";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }

    if(isset($_GET["edit"])){
        $update = true;
        $id= $_GET["edit"];
        $sql= "SELECT * FROM `kelas` WHERE id_kelas = $id";
        $q1 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($q1);

        $nama = $row["kelas"];
        $prodi = $row["prodi"];
        $fakultas = $row["fakultas"];

        if($nama == " "){
            $status = "kosong";
        }
    }

    if(isset($_POST["edit"])){
        $id = $_POST["id"];
        $nama = $_POST["kelas"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        $sql = "UPDATE `kelas` SET `kelas`='$nama',`prodi`='$prodi',`fakultas`='$fakultas' WHERE id_kelas = $id";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }

    if(isset($_GET["delete"])){
        $id = $_GET["delete"];
        $sql = "DELETE FROM `kelas` WHERE id_kelas=$id";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Dihapus";
        } else {
            $status = "Data Gagal Dihapus";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informasi Dosen</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<style>
 .nav-link{
     font-size: 20px;
     color : #f2f2f2;
    }
</style>

</head>
<body>
<nav class="navbar navbar-expand-sm bg-primary">
        <a class="navbar-brand" href="index.php"><img src="https://kompaspedia.kompas.id/wp-content/uploads/2020/08/logo_Universitas-Pendidikan-Ganesha-thumb.png" alt="Logo Undiksha" width="100 px"></a>
        <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="dosen.php">Dosen</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="kelas.php">Kelas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="jadwal.php">Jadwal</a>
        </li>
        </ul>
</nav>
<div class="container">
        <div class="row justify-content-center">
        <div class=" border-primary mt-3">
            <div class="container p-3 my-3 bg-primary text-white text-center">
                <h1>Daftar Kelas</h1>
                <p>Silahkan Masukkan Data Kelas Terlebih Dahulu</p>
            </div>  
    <form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
   <label for="Nama Kelas">Nama Kelas</label><br>
   <input type="text" class="form-control" name="kelas" placeholder="Nama Kelas" id="nama" required> <br>
   <label for="prodi">Program Studi</label><br>
            <input list="prodi" class="form-control" placeholder="Program Studi" name="prodi"><br>
                <datalist id="prodi">
                    <option value="Sistem Informasi">
                    <option value="Teknologi Pendidikan">
                    <option value="Desain Komunikasi Visual">
                    <option value="Manajemen">
                    <option value="Survey dan Pemetaan">
                    <option value="Ilmu Keolahragaan">
                    <option value="Kedokteran">
                    <option value="Analisis Kimia">
                </datalist>

    <label for="fakultas">Fakultas</label><br>
            <input list="fakultas" class="form-control" placeholder="Fakultas" name="fakultas">
                <datalist id="fakultas">
                    <option value="Fakultas Teknik dan Kejuruan">
                    <option value="Fakultas Ilmpu Pendidikan">
                    <option value="Fakultas Bahasa dan Seni">
                    <option value="Fakultas Ekonomi">
                    <option value="Fakultas Hukum dan Ilmu Sosial">
                    <option value="Fakultas Olahraga dan Kesehatan">
                    <option value="Fakultas Kedokteran">
                    <option value="Fakultas Matematika dan Ilmu Pengetahuan">
                </datalist>
                <br>

    <?php if($update == true):?>
    <input type="submit" class="btn btn-success " name="edit" value="Ubah">
    <?php else:?>
        <input type="submit" class="btn btn-success " name="submit" value="Simpan">
    <?php endif; ?>
    </form>

    <?php if(isset($_POST["submit"])) : ?>
        <div class="alert alert-primary">
            <?php
                echo "$status";
            ?>
        </div>
    <?php endif;?>

    <div class="container col-15">
        <?php
            include'conect.php';
            $sql = "SELECT * FROM kelas";
            $result = mysqli_query($conn,$sql);
        ?>
        <div class="row justify-content-center">
        <table class="table table-striped table-hover">
            <thead>
            <tr class= "table-primary">
                    <th>Kelas</th>
                    <th>Prodi</th>
                    <th>Fakultas</th>
                    <th>Action</th>
                </tr>
            </thead>
                <?php while($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row["kelas"]?></td>
                    <td><?php echo $row["prodi"]?></td>
                    <td><?php echo $row["fakultas"]?></td>
                    <td>
                        <a href="kelas.php?edit=<?php echo $row["id_kelas"];?>" class="btn btn-primary" >Edit</a>
                        <a href="kelas.php?delete=<?php echo $row["id_kelas"];?>" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            <?php endwhile;?>
        </table>
        </div>
    </div>

</body>
</html>